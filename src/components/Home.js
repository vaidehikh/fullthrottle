import React, { Component } from "react";
import { Switch, Route, withRouter } from 'react-router-dom';
import Welcome from "./Welcome";
import Activity from "./Activity";

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Switch>
                    <Route path='/' exact component={Welcome} />
                    <Route path='/activitypage/:itemId' exact component={Activity} />
                </Switch>
            </div>
        )
    }
}

export default withRouter(Home);

