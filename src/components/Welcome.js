import React, { Component } from "react";
import BackgroundHome from '../assets/backgroundImages/homePageBackground1.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { getUserList } from "../actions/Actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

const styles = {
    header: {
        backgroundImage: `url(${BackgroundHome})`,
        margin: '0px auto',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        // display: "flex",
        // justifyContent: "center",
        // alignItems: "center",
     },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        color: 'white',
        padding: "10px",
        // display: "flex",
        // justifyContent: "center",
        // alignItems: "center",

    }
}

class Welcome extends Component {
    constructor(props) {
        super(props);
        this.userClick = this.userClick.bind(this)
    }
    componentDidMount(props) {

        this.props.dispatch(getUserList());

    }
    userClick(itemId){
       
        
        this.props.history.push(`/activitypage/${itemId}`);
        
      }
    render() {
        return (
            console.log(this.props.userList.members),
            <>
                <div style={styles.header}>
                <div style={{ height: "80px", paddingTop:'50px'}}><h1>Welcome to check the user activities</h1></div>
                 {
                        this.props.userList.members && Object.values(this.props.userList.members).map((item, i) => (
                            console.log(item.real_name), 
                            <div style={{display: "flex", justifyContent: "center", alignItems: "center", paddingTop:'10px'}}>
                            
                            <Card style={styles.content}>
                            <CardContent>
                                <Typography gutterBottom  onClick={() => this.userClick(item.id)}>
                                {item.real_name}
                                        </Typography>
                                {/* <Typography gutterBottom style={{ fontSize: 20 }}>
                                    hellosdfsdff
                                        </Typography> */}
                         </CardContent>
                       </Card>
                        
                     </div>
                      ))

                     } 
                </div>
            </>
        )
    }
}

const select = (state) => ({
    userList: state.user.userList,

});

export default withRouter(connect(select)(Welcome));

