import React, { Component } from 'react'
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

const styles = {
    content: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        paddingTop: "150px"
    }
}

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
class ActivityChart extends Component {
    constructor(props) {
        super(props)

        this.state = {
            options: {
                title: {
                    text: 'Activity Of ' + this.props.name
                },
                chart: {
                    zoomType: 'x'
                },

                xAxis: {
                    type: 'category'
                },
                yAxis: {

                    title: {
                        text: 'Active Time Duration In Miniute'
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {

                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f} min'
                        }
                    }

                },

                series: [{
                    type: 'column',
                    data:

                        this.props.values.map((item) => (
                            [(new Intl.DateTimeFormat('en-GB', {
                                month: 'numeric',
                                day: '2-digit',
                                year: 'numeric',
                            }).format(new Date(item.start_time.split(" ").slice(0, -1)))),
                            ((new Date(Date.UTC(item.start_time.split(" ")[2], months.indexOf(item.start_time.split(" ")[0]), item.start_time.split(" ")[1], item.start_time.split(" ")[4].slice(-2) == "AM" ? item.start_time.split(" ")[4].split(":")[0] : item.start_time.split(" ")[4].split(":")[0]++ + 12, item.start_time.split(" ")[4].split(":")[1].slice(0, 2), 0))) - (new Date(Date.UTC(item.end_time.split(" ")[2], months.indexOf(item.end_time.split(" ")[0]), item.end_time.split(" ")[1], item.end_time.split(" ")[3].slice(-2) == "AM" ? item.end_time.split(" ")[3].split(":")[0] : item.end_time.split(" ")[3].split(":")[0]++ + 12, item.end_time.split(" ")[3].split(":")[1].slice(0, 2), 0)))) / 60000 * -1]
                        ))
                }]
            },
        };
    }

    render() {
        const { options } = this.state;
        return (
            <div style={styles.content}>
                <HighchartsReact
                    highcharts={Highcharts}
                    options={options}
                />
            </div>

        )
    }
}

export default ActivityChart