import React, { Component } from "react";
import BackgroundHome from '../assets/backgroundImages/homePageBackground1.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { getUserList } from "../actions/Actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import ActivityChart from "./ActivityChart";
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';

const styles = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {

        width: 200,
    },
    header: {
        backgroundImage: `url(${BackgroundHome})`,
        margin: '0px auto',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: 'white',
        paddingTop: "20px"

    }
}

class Activity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: '02/01/2020',
        }
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    handleDateChange(date) {
        this.setState({
            selectedDate: date
        });

    }
    componentDidMount(props) {

        this.props.dispatch(getUserList());

    }

    render() {
        const { selectedDate } = this.state;
        return (
            console.log(this.props.match.params.itemId),
            <>
                <div style={styles.header}>
                    {
                        this.props.userList.members && Object.values(this.props.userList.members).map((item, i) => (
                            console.log(item.real_name),
                            this.props.match.params.itemId == item.id ?
                                <div style={styles.content}>

                                    <ActivityChart values={item.activity_periods} name={item.real_name} />

                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker style={styles.content}
                                            margin="normal"
                                            id="date-picker-dialog"
                                            label="Select the date"
                                            format="MM/dd/yyyy"
                                            value={selectedDate}
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                        />
                                    </MuiPickersUtilsProvider>
                                    {
                                        item.activity_periods && Object.values(item.activity_periods).map((data, i) => (
                                            console.log(selectedDate),
                                            (new Intl.DateTimeFormat('en-GB', {
                                                month: 'numeric',
                                                day: '2-digit',
                                                year: 'numeric',
                                            }).format(new Date(data.start_time.split(" ").slice(0, -1)))) === (new Intl.DateTimeFormat('en-GB', {
                                                month: 'numeric',
                                                day: '2-digit',
                                                year: 'numeric',
                                            }).format(new Date(selectedDate))) ?
                                                <div style={{
                                                    display: "flex",
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                }}>
                                                    <Card style={styles.content}>
                                                        <CardContent>
                                                            <Typography gutterBottom >
                                                                Start Time {data.start_time}
                                                            </Typography>
                                                            <Typography gutterBottom >
                                                                End Time {data.end_time}
                                                            </Typography>
                                                        </CardContent>
                                                    </Card>
                                                </div>
                                                :
                                                <div>
                                                    <h6>Select the date to check the activity of {data.start_time.slice(0, -7)}</h6>
                                                </div>
                                        ))
                                    }

                                </div>

                                : <div></div>
                        ))

                    }
                </div>
            </>
        )
    }
}

const select = (state) => ({
    userList: state.user.userList,
});

export default withRouter(connect(select)(Activity));

