import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import user from './reducers/user-reducer';

const reducers = combineReducers({
 user
});
const initialState = {};
const enhancers = [];
const middleware = [thunk];

const { devToolsExtension } = window;
if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
const store = createStore(reducers, initialState, composedEnhancers);

export default store;