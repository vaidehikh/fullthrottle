export function getUserListApi() {

    console.log("getUserListApi method call")
        ;
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',

    };

    return fetch(`https://run.mocky.io/v3/25a7f890-3f72-4fbd-bafb-eb95b2114222`, requestOptions)

        .then((res) => {
            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
            return res.json();
        })
        
        .catch(err => err);
}