# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up :-
   Clone the project in your desire directory using htttps copy the path and clone 

* Configuration

* Dependencies :-
  "@date-io/date-fns": "1.x",
    "@material-ui/core": "^4.11.0",
    "@material-ui/pickers": "^3.2.10",
    "@testing-library/jest-dom": "^4.2.4",
    "@testing-library/react": "^9.3.2",
    "@testing-library/user-event": "^7.1.2",
    "date-fns": "^2.0.0-beta.5",
    "highcharts": "^8.2.0",
    "highcharts-react-official": "^3.0.0",
    "history": "^5.0.0",
    "moment": "^2.27.0",
    "react": "^16.13.1",
    "react-charts": "^2.0.0-beta.7",
    "react-dom": "^16.13.1",
    "react-redux": "^7.2.1",
    "react-router-dom": "^5.2.0",
    "react-scripts": "3.4.3",
    "redux": "^4.0.5",
    "redux-thunk": "^2.3.0"

   
* Database configuration:-
 uses the mock api for fetching json

* How to run tests :-
  yarn start
  
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact